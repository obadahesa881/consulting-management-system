<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CategorieExpertController;
use App\Http\Controllers\ExpertController;
use App\Http\Controllers\ImageController;
use App\Http\Controllers\OredersController;
use App\Http\Controllers\TimeController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::controller(AuthController::class)->group(function () {
    Route::post('login', 'login');              // done
    Route::post('register', 'register');        //done
    Route::post('logout', 'logout');            //done
});

Route::middleware('custom-jwt')->group(function () {
    Route::get('getAllExpert', [\App\Http\Controllers\ExpertController::class, 'getAllExperts']);
});

Route::controller(CategorieExpertController::class)->group(function () {
    Route::get('getAllExpertByCategorie', 'getAllExpertByCategorie'); //done
});
Route::controller(UserController::class)->group(function () {
    Route::post('UpdateUserPorfile', 'UpdateUserPorfile');      //done
    Route::get('UserProfile', 'UserProfile');   //done
});

Route::controller(ExpertController::class)->group(function () {
    Route::get('getAllExperts', 'getAllExperts');        //done
    Route::post('ExpertPorfile', 'ExpertPorfile');         //done 
    Route::post('UpdateExpertPorfile', 'UpdateExpertPorfile'); //done 
});

Route::controller(TimeController::class)->group(function () {
    Route::post('createTimeTable', 'createTimeTable'); //done
    Route::get('getTime', 'getTime');           //done
    Route::post('updateTimeTable', 'updateTimeTable'); //done
    Route::post('reserveTime', 'reserveTime');      //done
    Route::post('createReserveTime', 'createReserveTime'); //done
});

Route::post('image', [ImageController::class, 'imageStore']); //done => testing

Route::get('getOrders', [OredersController::class, 'getOrders']); //done => testing

Route::post('search', [SearchController::class, 'search']);
