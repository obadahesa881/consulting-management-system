<?php

namespace Database\Seeders;

use App\Models\Categorie;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Field::truncate();

        Categorie::create([
            'title' => 'not specified',
        ]);
        Categorie::create([
            'title' => 'Medical',
        ]);
        Categorie::create([
            'title' => 'Vocational',
        ]);
        Categorie::create([
            'title' => 'Psychological',
        ]);
        Categorie::create([
            'title' => 'family',
        ]);
        Categorie::create([
            'title' => 'Business/Management',
        ]);
        // php artisan db:seed --class=CategorySeeder
        // php artisan db:seed
    }
}
