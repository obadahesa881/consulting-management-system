<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Categorie extends Model
{
    protected $primaryKey = 'categorie_id';
    // protected $table = "categories";
    public $timestamps = false;
    protected $fillable = [
        'categorie_id',
        'title'
    ];
    public function experts()
    {
        return $this->belongsToMany(Expert::class, 'categorie_expert', 'categorie_id', 'expert_id');
    }
    use HasFactory;
}
