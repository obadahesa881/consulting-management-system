<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Time extends Model
{
    protected $primaryKey = 'expert_id';
    public $incrementing = false;
    public $timestamps = false;
    protected $fillable = [
        'expert_id',
        'reservation_price',
        'day',
        'from',
        'to'
    ];
    use HasFactory;

    public function expert()
    {
        return $this->belongsTo(Expert::class, 'expert_id');
    }
}
