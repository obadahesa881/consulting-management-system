<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Expert extends Model
{

    protected $primaryKey = 'expert_id';
    public $incrementing = false;
    public $timestamps = false;

    use HasFactory;

    protected $fillable = [
        'expert_id',
        'address',
        'phone',
        'description',
    ];
    public function user()
    {
        return $this->hasOne(User::class, 'id', 'expert_id');
    }
    public function categories()
    {
        return $this->belongsToMany(Categorie::class, 'categorie_expert', 'expert_id', 'categorie_id');
    }
    public function time()
    {
        return $this->hasMany(Time::class, 'expert_id');
    }
    public function reservation_1()
    {
        return $this->belongsToMany(User::class, 'reservations', 'user_id', 'expert_id');
    }
}
