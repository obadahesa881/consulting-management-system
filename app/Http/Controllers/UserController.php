<?php

namespace App\Http\Controllers;

use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function UpdateUserPorfile(Request $request)
    {
        // $user_id = optional(Auth::user())->id;
        // if (!$user_id == null) {

        //     $user = User::find($user_id);
        //     $user->update([
        //         'first_name' => $request->first_name,
        //         'last_name' => $request->last_name,
        //         'email' => $request->email,
        //         'password' => Hash::make($request->password),
        //     ]);
        //     return response([
        //         'message' => 'success',
        //         'expert:' => new UserResource($user),
        //     ], 200);
        // }
        // return response([
        //     'message' => 'not valid Token',
        // ], 401);
        $expert = array();
        $expert = $request->expert;
        dd($expert);
        foreach ($expert as $e) {
            return response([
                'message' => $e
            ]);
        }
    }
    //done => testing
    public function UserProfile()
    {
        $user_id = optional(Auth::user())->id;
        if (!$user_id == null) {
            $user = User::find($user_id);
            return response([
                'user_info' => new UserResource($user)
            ], 200);
        }
        return response([
            'message' => 'not valid token'
        ], 401);
    }
}
