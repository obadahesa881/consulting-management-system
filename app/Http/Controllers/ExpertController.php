<?php

namespace App\Http\Controllers;

use App\Http\Resources\ExpertResource;
use App\Models\Categorie;
use App\Models\Expert;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use PHPOpenSourceSaver\JWTAuth\Facades\JWTAuth;

class ExpertController extends Controller
{
    // Bassam
    public function getAllExperts()
    {
        $experts = Expert::all();
        foreach ($experts as $item) {
            $user = User::where("id", "=", $item->expert_id)->first();
            $categorie =  $item->categories()->get();
            $item['user'] = $user;
            $item['categorie'] = $categorie;
        }
        return $experts;
    }
    //done => testing = oone
    public function ExpertPorfile(Request $request)
    {
        $user_id = optional(Auth::user())->id;
        if (!$user_id == null) {

            $category = array();
            $medical = $request->medical;
            if ($medical != 0) {
                $category[0] = $medical;
            }
            $vacitonal = $request->vacitonal;
            if ($vacitonal != 0) {
                $category[1] = $vacitonal;
            }
            $family = $request->family;
            if ($family != 0) {
                $category[2] = $family;
            }
            $business = $request->business;
            if ($business != 0) {
                $category[3] = $business;
            }
            $phsycological = $request->phsycological;
            if ($phsycological != 0) {
                $category[4] = $phsycological;
            }

            $expert = Expert::create([
                'expert_id' => $user_id,
                'address' => $request->address,
                'phone' => $request->phone,
                'description' => $request->description,
            ]);
            $expert->categories()->attach($category);   // solved by omran >_<

            return response([
                'message' => 'success',
                'expert:' => new ExpertResource($expert),
                'cats' => $expert->categories->toArray()
            ], 200);
        }
        return response([
            'massage' => 'not valid token'
        ], 401);
    }
    //done => testing = done 
    public function UpdateExpertPorfile(Request $request)
    {
        $user_id = optional(Auth::user())->id;
        if (!$user_id == null) {
            $expert = Expert::find($user_id);
            $expert->categories()->detach();
            $categorie_id = $request->categorie_id;
            $expert->update([
                'address' => $request->address,
                'phone' => $request->phone,
                'description' => $request->description,
            ]);
            $expert->categories()->attach($categorie_id);   // solved by omran >_<
            return response([
                'message' => 'success',
                'expert:' => new ExpertResource($expert),
            ], 200);
        }
        return response([
            'massage' => 'not valid token'
        ], 401);
    }
}
