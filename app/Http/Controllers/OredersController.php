<?php

namespace App\Http\Controllers;

use App\Models\Reservation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OredersController extends Controller
{
    public function getOrders()
    {
        $user_id = optional(Auth::user())->id;
        if (!$user_id == null) {
            $account_type = optional(Auth::user())->account_type;
            if ($account_type == 0) {
                #user
                $orders = Reservation::where('user_id', $user_id)->get();
                if ($orders == null) {
                    return response([
                        'message' => 'there is no orders to show'
                    ], 200);
                }
            } elseif ($account_type == 1) {
                #expert
                $orders = Reservation::where('expert_id', $user_id)->whereNotNull('user_id')->get();
                if ($orders == null) {
                    return response([
                        'message' => 'there is no orders to show'
                    ], 200);
                }
            }
            return response([
                'message' => 'success',
                'oreders' => $orders
            ], 200);
        }
        return response([
            'massage' => 'not valid token'
        ], 401);
    }
}
