<?php

namespace App\Http\Controllers;

use App\Models\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class ImageController extends Controller
{
    public function imageStore(Request $request)
    {
        $user_id = optional(Auth::user())->id;
        if ($user_id != null) {

            $this->validate($request, [
                'image' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',
            ]);
            $image_path = $request->file('image')->store('image', 'public');

            $data = Image::create([
                'image_id' => $user_id,
                'image' => $image_path,
            ]);

            return response($data, Response::HTTP_CREATED);
        }
        return response([
            'message' => 'invalid token',
        ], 401);
    }
}
