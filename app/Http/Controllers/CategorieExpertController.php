<?php

namespace App\Http\Controllers;

use App\Models\Categorie;

use App\Models\Expert;
use Illuminate\Http\Request;

class CategorieExpertController extends Controller
{
    //done => testing
    public function getAllExpertByCategorie(Request $request)
    {
        $category = Categorie::with('experts.user')->find($request->categorie_id);
        return response([
            'message' => $category
        ], 200);
    }
}
