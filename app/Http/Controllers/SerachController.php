<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class SearchController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api');
    }
    public function search(Request $request)
    {
        $name = $request->first_name;
        $result = DB::select("SELECT users.first_name, users.last_name, categories.title
        FROM users JOIN experts
        ON users.id = experts.user_id
        JOIN expert_categories
        ON experts.id = expert_categories.expert_id
        JOIN categories
        ON categories.id = expert_categories.category_id
        WHERE users.first_name LIKE '%$name%' AND is_expert = 1;");

        if ($result)
            return response()->json([
                'status' => 'done',
                'result' => $result
            ]);

        return response()->json([
            'status' => 'Not found'
        ]);
    }
}
