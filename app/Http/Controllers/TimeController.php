<?php
//for obadah
namespace App\Http\Controllers;

use App\Models\Expert;
use App\Models\Reservation;
use App\Models\Time;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TimeController extends Controller
{   //done =>testing done
    public function createTimeTable(Request $request)
    {
        $request->validate([
            'day' => 'string|min:1|max:7',
            'from' => 'string|min:8|max:24',
            'to' => 'string|min:8|max:24|gt:from',
        ]);
        $day1 = $request->day1;
        $day2 = $request->day2;
        $day3 = $request->day3;
        $day4 = $request->day4;
        $day5 = $request->day5;
        $day6 = $request->day6;
        $day7 = $request->day7;
        $from1 = $request->from1;
        $from2 = $request->from2;
        $from3 = $request->from3;
        $from4 = $request->from4;
        $from5 = $request->from5;
        $from6 = $request->from6;
        $from7 = $request->from7;
        $to1 = $request->to1;
        $to2 = $request->to2;
        $to3 = $request->to3;
        $to4 = $request->to4;
        $to5 = $request->to5;
        $to6 = $request->to6;
        $to7 = $request->to7;
        $reservation_price = $request->reservation_price;
        $k = 0;
        $arr = array($day1, $day2, $day3, $day4, $day5, $day6, $day7);
        $arr2 = array($from1, $to1, $from2, $to2, $from3, $to3, $from4, $to4, $from5, $to5, $from6, $to6, $from7, $to7);
        $expert_id = optional(Auth::user())->id;
        if (!$expert_id == null) {
            for ($i = 0; $i < sizeof($arr); $i++) {
                $day = $arr[$i];
                if ($arr2[$k] == null) {
                    $k += 2;
                    continue;
                }
                $from = $arr2[$k];
                $to = $arr2[$k + 1];
                $k += 2;
                $time = Time::create([
                    'expert_id' => $expert_id,
                    'day' => $day,
                    'from' => $from,
                    'to' => $to,
                    'reservation_price' => $reservation_price,
                ]);
            }
            return response([
                'message' => 'success',
                'time' => $time,
            ], 200);
        }
        return response([
            'message' => 'not valid Token',
        ], 401);
    }
    //done => testing done
    public function updateTimeTable(Request $request)
    {
        $expert_id = optional(Auth::user())->id;
        if (!$expert_id == null) {
            $time = Time::find($expert_id);
            $time->update([
                'day-1' => $request->day,
                'from' => $request->from,
                'to' => $request->to,
                'reservation_price' => $request->reservation_price,
            ]);
            return response([
                'message' => 'success',
                'time' => $time,
            ], 200);
        }
        return response([
            'message' => 'not valid Token',
        ], 401);
    }
    //done => testing done
    public function getTime()
    {
        $expert_id = optional(Auth::user())->id;
        if (!$expert_id == null) {
            $time = Time::find($expert_id);
            return response([
                'message' => 'success',
                'time' => $time,
            ], 200);
        }
        return response([
            'message' => 'not valid Token',
        ], 401);
    }
    //done => testing done
    public function createReserveTime(Request $request)
    {
        $expert_id = $request->expert_id;
        $times = collect(new Reservation());
        $expertTime = Time::where('expert_id', $expert_id)->get();
        $reservation = Reservation::where('expert_id', $expert_id)->get();
        if (!$reservation->isnotempty()) {
            foreach ($expertTime as $e) {
                for ($i = $e->from; $i < $e->to; $i++) {
                    $reservation = Reservation::create([
                        'expert_id' => $expert_id,
                        'user_id' => null,
                        'day' => $e->day,
                        'from' => $i,
                        'to' => $i + 1
                    ]);
                    $times->push($reservation);
                }
            }
        } else {
            foreach ($reservation as $res) {
                if ($res->user_id == 0) {
                    $times->push($res);
                }
            }
        }

        return response([
            'all reservations available' => $times,
            'all reservations price' => Time::where('expert_id', $expert_id)->first()->reservation_price
        ], 200);
    }
    //done => testing
    public function reserveTime(Request $request)
    {
        $user_id = optional(Auth::user())->id;
        $expert_id = $request->expert_id;
        $from = $request->from;
        $day = $request->day;
        if ($user_id != null) {
            $reservation = Reservation::where('expert_id', $expert_id)->where('day', $day)->where('from', $from)->first();
            $reservation->update(['user_id' => $user_id]);
            //price
            $reservation_price = Time::where('expert_id', $expert_id)->first();
            $user = User::where('id', $user_id)->first();
            $expert_info = Expert::where('expert_id', $expert_id)->first()->user;
            if ($user->balance >= $reservation_price->reservation_price) {
                $gain = $expert_info->balance + $reservation_price->reservation_price;
                $pay = $user->balance - $reservation_price->reservation_price;
                $user->update(['balance' => $pay]);
                $expert_info->update(['balance' => $gain]);

                return response([
                    'message' => 'success',
                    'appotiment' => $reservation,
                    'your current balance is' => $user->balance
                ], 200);
            }
            return response([
                'message' => 'not enough money',
                'your current balance is ' => $user->balance,
            ], 402);
        }
        return response([
            'message' => 'not valid Token',
        ], 401);
    }
}
